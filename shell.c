#include "shell.h"

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "write_all.h"
#include "utils.h"
#include "stream_reader.h"

const char g_shell_prompt[] = "UwU foxshell> ";

static void print_prompt()
{
	put_string(g_orange_color);
	put_string(g_shell_prompt);
	put_string(g_reset_color);
}


static int execve_program(char* argv[])
{
    int r;
    int status;
    pid_t fpid; 
    char* envp[] = {NULL};

    fpid = fork();
    if (fpid < 0)
    {
        perror();
        return -1;
    }
    else if (fpid == 0)
    {
        r = execve(argv[0], argv, envp);
        if (r < 0) {
            perror();
        }
        exit(1);
    }
    else 
    {
        r = wait(&status);
        if (r < 0) {
           perror();
        }
        return status;
    }
}

int run_shell()
{
    t_token_list* tl;
    t_token_list* tl_it;
    int argc;
    int i;
    char** argv;

	while (1)
	{
		print_prompt();
		tl = read_tokens();
        if (!tl) {
            put_string("\n");
            break;
        }
        
        argc = token_list_length(tl);
        argv = malloc(sizeof(*argv) * (argc + 1));

        tl_it = tl;
        i = 0;
        while (tl_it)
        {
            argv[i] = tl_it->token.ptr;

            tl_it = tl_it->next;
            ++i;
        }
        argv[i] = NULL;
        execve_program(argv);

        free(argv);
        token_list_destroy(tl);
	}
	return 0;
}
