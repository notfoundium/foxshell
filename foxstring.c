#include <stdlib.h>

#include "foxstring.h"
#include "utils.h"


void foxstring_init(t_foxstring* foxstring, const char* content)
{
	int len;
	int i;

	len = fox_strlen(content);

	foxstring->length = len;
	foxstring->reserved = len + 1;
	foxstring->ptr = malloc(len + 1);
	i = 0;
	while (i < len + 1) {
		foxstring->ptr[i] = content[i];
		++i;
	}
}

void foxstring_push(t_foxstring* foxstring, char c)
{
	if (foxstring->length + 1 == foxstring->reserved)
	{
		foxstring->ptr = fox_realloc(foxstring->ptr, foxstring->reserved, foxstring->reserved * 2);
		foxstring->reserved *= 2;
	}
	foxstring->ptr[foxstring->length++] = c;
    foxstring->ptr[foxstring->length] = 0   ;
}

void foxstring_destruct(t_foxstring* foxstring)
{
	free(foxstring->ptr);
}