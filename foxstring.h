#ifndef FOXSTRING_H
# define FOXSTRING_H

typedef struct s_foxstring
{
	char* ptr;
	int length;
	int reserved;
} t_foxstring;

void foxstring_init(t_foxstring* foxstring, const char* content);
void foxstring_push(t_foxstring* foxstring, char c);
void foxstring_destruct(t_foxstring* foxstring);

#endif