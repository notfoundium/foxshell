#include <unistd.h>


int write_all(int fd, const char* s, int sz)
{
	int printed = 0;

	while (sz > 0)
	{
		int r = write(fd, s, sz);
		if (r < 0)
		{
			return -1;
		}
		sz -= r;
		s += r;
		printed += r;
	}
	return printed;
}
