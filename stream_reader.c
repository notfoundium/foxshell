#include <unistd.h>
#include <stdlib.h>

#include "stream_reader.h"
#include "foxstring.h"
#include "utils.h"

static int fox_getch()
{
	char c;
	int r = read(0, &c, 1);
	
	if (r > 0)
		return c;
	return r - 1;
}

t_token_list* read_tokens()
{
	int symb;
	t_token_list* token_list;
	t_token_list* current_token;
	t_read_mode rmode;
    char is_escaped;
    char is_space; 
    char is_last_space;
    int read_symbols;

    read_symbols = 0;
    is_escaped = 0;
    is_last_space = 1;
	rmode = DEFAULT;
	token_list = malloc(sizeof(*token_list));
	token_list->next = NULL;
	current_token = token_list;
	foxstring_init(&current_token->token, "");
	while ((symb = fox_getch()) > 0)
	{
        ++read_symbols;
		if (rmode == DEFAULT)
		{   
            is_space = 0;
            if (is_escaped)
            {
                (symb != '\n' ? foxstring_push(&current_token->token, symb) : 0);
                is_escaped = 0;
            }
			else if (symb == ' ' || symb == '\n')
			{
				if (symb == '\n')
					break;
                if (!is_last_space)
                {
                    current_token->next = malloc(sizeof(*current_token));
                    current_token = current_token->next;
                    current_token->next = NULL;
                    foxstring_init(&current_token->token, "");
                }
                is_space = 1;
			}
			else if (symb == '\"')
			{
				rmode = SOFT_MODE;
			}
			else if (symb == '\'')
			{
				rmode = HARD_MODE;
			}
            else if (symb == '\\')
            {
                is_escaped = 1;
            }
			else {
				foxstring_push(&current_token->token, symb);
			}
            is_last_space = is_space;
		}
		else if (rmode == SOFT_MODE)
		{
            if (is_escaped)
            {
                (symb != '\n' ? foxstring_push(&current_token->token, symb) : 0);
                is_escaped = 0;
            }
			else if (symb == '\"')
			{
				rmode = DEFAULT;
			}
            else if (symb == '\\')
            {
                is_escaped = 1;
            }
			else
			{
				foxstring_push(&current_token->token, symb);
			}
		}
		else
		{
			if (symb == '\'')
			{
				rmode = DEFAULT;
			}
			else
			{
				foxstring_push(&current_token->token, symb);
			}
		}

        if (symb == '\n')
        {
            put_string(g_orange_color);
            put_string("> ");
            put_string(g_reset_color);
        }
	}
    if (read_symbols)
	    return token_list;
    token_list_destroy(token_list);
    return NULL;
}

void token_list_destroy(t_token_list* token_list)
{
    if (!token_list)
        return;
    
    token_list_destroy(token_list->next);
    foxstring_destruct(&token_list->token);
    free(token_list);
}

int token_list_length(t_token_list* token_list)
{
    int result;

    result = 0;
    while (token_list) {
        ++result;
        token_list = token_list->next;
    }
    return result;
}
