#ifndef UTILS_H
# define UTILS_H

void perror();
void put_string(const char* str);
int fox_strlen(const char* s);
void* fox_realloc(void* content, int old_size, int new_size);

extern const char g_reset_color[];
extern const char g_orange_color[];

#endif