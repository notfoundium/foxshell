#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "utils.h"
#include "write_all.h"

const char g_reset_color[] = "\x01b[0m";
const char g_orange_color[] = "\x01b[91m";

void perror()
{
	const char* error = strerror(errno);
	write_all(1, "Error occured: ", 15);
	write_all(1, error, fox_strlen(error));
	write_all(1, "\n", 1);
}

void put_string(const char* str)
{
	write_all(1, str, fox_strlen(str));
}

int fox_strlen(const char* s)
{
	int result = 0;
	while (*(s++))
	{
		++result;
	}

	return result;
}

void* fox_realloc(void* content, int old_size, int new_size)
{
	char* old_cont = content;
	void* result = malloc(new_size);
	char* new_cont = result;

	while (old_size && new_size) {
		*new_cont = *old_cont;
		++new_cont;
		++old_cont;
		--old_size;
		--new_size;
	}
	free(content);
	return result;
}
