NAME		=	foxshell

CC			=	clang
CC_STD		=	-std=c99
CC_FLAGS	=	#-Wall -Wextra

SRC			=	*.c

all:
	$(CC) $(CC_STD) $(CC_FLAGS) $(SRC) -o $(NAME)

clean:
	rm -rf *.o

fclean: clean
	rm $(NAME)

re: fclean all
