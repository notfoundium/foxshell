#ifndef STREAM_READER_H
# define STREAM_READER_H

# include "foxstring.h"

typedef struct s_token_list
{
	t_foxstring token;
	struct s_token_list* next;
} t_token_list;

typedef enum e_read_mode
{
	DEFAULT,
	SOFT_MODE,
	HARD_MODE
} t_read_mode;

t_token_list* read_tokens();
void token_list_destroy(t_token_list* token_list);
int token_list_length(t_token_list* token_list);

#endif