#ifndef WRITE_ALL_H
# define WRITE_ALL_H

int write_all(int fd, const char* s, int sz);

#endif